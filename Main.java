
public class Main {

	public static void main(String[] args) {
		short[][] bev = new short[25000000][6];
		int counter = 0;
		int days = 0;
		int total = 0;
		int diff = 0;
		
		//initialization of bev 
		for (int i = 0; i < bev.length; i++) {
			short n = (short) (Math.random() * 5);
			bev[i][0] = (short) (n + 1);
			for (int j = 1; j < 6; j++) {
				if(j - 1 > n) bev[i][j] = -1;
			}
		}
		for (int i = 0; i < 1000; i++) {
			if(bev[i][1] != -1) {
				bev[i][1] = 1;
				counter++;
			}
		}

		
		//loop through every day till no infection
		while(counter > 0) {
			System.out.println("Day: " + days + " Counter: " +  counter);
			for (int i = 0; i < bev.length; i++) {
				for (int j = 1; j < 6; j++) {
					if(bev[i][j] > 0 && bev[i][j] < diff) {
						//waiting till you get infectious
						bev[i][j]++;
					} else if(bev[i][j] == diff + 1 || bev[i][j] == diff + 2) {
						// you are infectious
						for (int j2 = 1; j2 < 6; j2++) {
							if(bev[i][j2] == 0) {
								bev[i][j2]++;
								counter++;
							}
						}
						//infecting other possible person
						int perc = (int) (Math.random() * 100);
						if(perc >= 65) {
							int perc2 = (int) (Math.random() * 25000000);
							int n = bev[perc2][0];
							int perc3 = (int) (Math.random() * n);
							if(bev[perc2][perc3] == 0) {
								bev[perc2][perc3]++;
								counter++;
							}
							
						}
						bev[i][j]++;
						if(bev[i][j] == diff + 3) {
							counter--;
							total++;
						}
					}
				}
			}
			days++;
		}
		System.out.println("Total cases: " + total);
	}
	

}
