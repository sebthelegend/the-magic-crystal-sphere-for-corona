import matplotlib.pyplot as plt
import numpy as np

# number of days to simulate
maxd = 1000

'''
    function: model SEIR with 6 cities and one unified rural area
    float a: period between infected and infectious
    float gamma: period of I
    list int cp: population of the cities
    int lp: population of the rural area
    float ct: beta value for people going to cities
    float lt: beta value for people going to the rural area
    list float cd: individual beta value for the cities 
    float ld: individual beta value for the rural area
    list int ci: I at the start for each area
'''
def model_SEIRGeo(a, gamma, cp, lp, ct, lt, cd, ld, ci):
    # initializing all variables and lists
    d = 1
    i_temp = ci.copy()
    s_temp = cp.copy()
    counter = 0
    while counter < 6:
        s_temp[counter] -= ci[counter]
        counter += 1
    s_temp += [lp - ci[6]]
    r_temp = [0,0,0,0,0,0,0]
    e_temp = [0,0,0,0,0,0,0]
    l_temp = [0,0,0,0,0,0,0]
    ys = [[s_temp[0]],[s_temp[1]],[s_temp[2]],[s_temp[3]],[s_temp[4]],[s_temp[5]],[s_temp[6]]]
    ye = [[0],[0],[0],[0],[0],[0],[0]]
    yi = [[ci[0]],[ci[1]],[ci[2]],[ci[3]],[ci[4]],[ci[5]],[ci[6]]]
    yr = [[0],[0],[0],[0],[0],[0],[0]]
    yl = [[0],[0],[0],[0],[0],[0],[0]]
    adds = [s_temp[0] + s_temp[1] + s_temp[2] + s_temp[3] + s_temp[4] + s_temp[5] + s_temp[6]]
    adde = [0]
    addi = [ci[0] + ci[1] + ci[2] + ci[3] + ci[4] + ci[5] + ci[6]]
    addr = [0]
    addl = [0]
    # loop through all days
    while d < maxd:
        counter = 0
        addi += [0]
        adds += [0]
        addr += [0]
        adde += [0]
        addl += [0]
        ins = [0,0,0,0,0,0,0]
        ine = [0,0,0,0,0,0,0]
        ini = [0,0,0,0,0,0,0]
        inr = [0,0,0,0,0,0,0]
        # loop through all areas calculating transitions between all areas
        while counter < 7:
            # S, E, I and R going to other areas for the current area
            if counter < 6:
                ct_temp = ct
                ins[counter] = -5 * ct * s_temp[counter] - lt * s_temp[counter]
                ine[counter] = -5 * ct * e_temp[counter] - lt * e_temp[counter]
                ini[counter] = -5 * ct * i_temp[counter] - lt * i_temp[counter]
                inr[counter] = -5 * ct * r_temp[counter] - lt * r_temp[counter]
            else:
                # special case for the rural area
                ct_temp = lt
                ins[counter] = -6 * ct * s_temp[counter]
                ine[counter] = -6 * ct * e_temp[counter]
                ini[counter] = -6 * ct * i_temp[counter]
                inr[counter] = -6 * ct * r_temp[counter]
            # adding all visitors for the current area
            c = 0 
            while c < 7:
                if c != counter:
                    ins[counter] += ct_temp * s_temp[c]
                    ine[counter] += ct_temp * e_temp[c]
                    ini[counter] += ct_temp * i_temp[c]
                    inr[counter] += ct_temp * r_temp[c]
                c += 1
            counter += 1
        # loop through all areas 
        counter = 0
        while counter < 7:
            # calculating actual S, E, I, R, L for each area
            if counter == 6:
                sminus = ld * (i_temp[counter] + ini[counter]) * (s_temp[counter] + ins[counter])/(lp + ini[counter] + ine[counter] + ins[counter] + inr[counter])
            else:
                sminus = cd[counter] * (i_temp[counter] + ini[counter]) * (s_temp[counter] + ins[counter])/(cp[counter] + ini[counter] + ine[counter] + ins[counter] + inr[counter])
            iminus = gamma * (i_temp[counter] + ini[counter])
            eminus = a * (e_temp[counter] + ine[counter])
            e_temp[counter] += sminus - eminus
            i_temp[counter] += eminus - iminus
            rminus = iminus * l
            l_temp[counter] += rminus
            r_temp[counter] += iminus - rminus
            s_temp[counter] -= sminus
            if s_temp[counter] < 0 : 
                s_temp[counter] = 0
            yi[counter] += [i_temp[counter]]
            ys[counter] += [s_temp[counter]]
            yr[counter] += [r_temp[counter]]
            ye[counter] += [e_temp[counter]]
            yl[counter] += [l_temp[counter]]
            # sum of all areas
            addi[d] += yi[counter][d]
            adds[d] += ys[counter][d]
            addr[d] += yr[counter][d]
            adde[d] += ye[counter][d]
            addl[d] += yl[counter][d]
            counter += 1
        d += 1
    y1 = [yi[0], ye[0], ys[0], yr[0], yl[0]]
    y2 = [yi[1], ye[1], ys[1], yr[1], yl[1]]
    y3 = [yi[2], ye[2], ys[2], yr[2], yl[2]]
    y4 = [yi[3], ye[3], ys[3], yr[3], yl[3]]
    y5 = [yi[4], ye[4], ys[4], yr[4], yl[4]]
    y6 = [yi[5], ye[5], ys[5], yr[5], yl[5]]
    y7 = [yi[6], ye[6], ys[6], yr[6], yl[6]]
    addres = [addi, adds, addr, adde, addl]
    # returning all [I, E, S, R, L] for each area and the sum of all areas
    return y1, y2, y3, y4, y5, y6, y7, addres

# values for normal SEIR function please refer to program SEIR-model
i =  510
s = 8400000 - i
r = 0
e = 0
l = 0.047
# same function as in program SEIR-model this time without option to turn off all additions but immunity
def model_SEIR(a, beta, gamma, immunity):
    d = 1
    i_temp = i
    # put at the start 1/7 of people into the R group to simulate immunity
    if immunity:
        s_temp = s * 6 / 7
        r_temp = r + s * 1 / 7
    else:
        s_temp = s
        r_temp = r
    e_temp = e
    yi = [i]
    ys = [s]
    yr = [r]
    ye = [e]
    while d < maxd:
        sminus = beta * i_temp * s_temp/p
        iminus = gamma * i_temp
        eminus = a * e_temp
        e_temp += sminus - eminus
        i_temp += eminus - iminus
        r_temp += iminus
        s_temp -= sminus
        if s_temp < 0 : 
            s_temp = 0
        yi += [i_temp]
        ys += [s_temp]
        yr += [r_temp]
        ye += [e_temp]
        d += 1
    return yi, ys, yr, ye

# x values
counter = 0
x = []
while counter < maxd:
    x += [counter]
    counter += 1

plt.style.use('bmh')
fig = plt.figure()

plt1 = fig.add_subplot(211)
plt2 = fig.add_subplot(212)

# intializing all values
a1 = 0.5
gamma1 = 0.01
cp1 = [1500000, 1200000, 1000000, 1000000, 900000, 800000]
lp1 = 2000000
ct1 = 0.01
lt1 = 0.001
cd1 = [0.10, 0.09, 0.08, 0.06, 0.07, 0.05]
ld1 = 0.04
ci1 = [100, 50, 30, 100, 60, 20, 150]
yc1, yc2, yc3, yc4, yc5, yc6, ycl, ins = model_SEIRGeo(a1, gamma1, cp1, lp1, ct1, lt1, cd1, ld1, ci1)

# calculating actual population
p = lp1
counter = 0
while counter < 6:
    p += cp1[counter]
    counter += 1

#calculating acutal beta value
counter = 0
beta = (lp1 / p) * lt1
while counter < 6:
    beta += (cp1[counter] / p) * cd1[counter]
    counter += 1

plt1.plot(x, ycl[0], linestyle = 'dashed', label = "Rural")
plt1.plot(x, yc1[0], color = 'red', label = "City 1-6")
plt1.plot(x, yc2[0], color = 'red')
plt1.plot(x, yc3[0], color = 'red')
plt1.plot(x, yc4[0], color = 'red')
plt1.plot(x, yc5[0], color = 'red')
plt1.plot(x, yc6[0], color = 'red')
plt1.set_title(f'R: 6, beta: 0.06, gamma: {gamma1}, a: {a1}', fontsize=20)

i1, s1, r1, e1 = model_SEIR(a1, 0.06, gamma1, False)
i2, s2, r2, e2 = model_SEIR(a1, 0.06, gamma1, True)

plt2.plot(x, ins[0], label = 'with pop. density', linestyle = '-.')
plt2.plot(x, i1, label = 'without pop. density and 0 immunity', linestyle = ':')
plt2.plot(x, i2, label = 'without pop. density and 1/7 immunity',  linestyle = '-')
plt1.set_title(f'R: 6, beta: 0.06, gamma: {gamma1}, a: {a1}', fontsize=20)

plt.xlabel('days', fontsize=15)
plt.ylabel('people', fontsize = 15)
plt1.legend(prop={'size' : 17})
plt2.legend(prop={'size' : 17})
plt.show()