import matplotlib.pyplot as plt
''' 
    i : I at the start
    s : S at the start
    r : R at the start
    e : E at the start
    l : L at the start
    mu : death rate
    lam : birth rate
    maxd : day period
'''
i =  1000
s = 83000000 - i
r = 0
e = 0
l = 0.047
mu = 0.012/356
lam = 0.004/356
maxd = 1000

''' 
    function: model for specific variables 

    float a : period between infected and infectious
    float beta : people you meet in one day
    float gamma : period of I
    boolean seir : seir or sir model
    boolean lethal : with lethality or without
    boolean vitalD : with or without vital Dynamics
'''
def model_SIR(a, beta, gamma, seir, lethal,vitalD):
    # initializing all counters and lists
    d = 0
    i_temp = i
    s_temp = s
    r_temp = r
    e_temp = e
    if lethal:
        l_temp = 0
        yl = [l_temp]
    yi = [i]
    ys = [s]
    yr = [r]
    ye = [e]

    # loop through all days
    while d < maxd:
        # calculating new values for S, E, I, R, L
        sminus = beta * i_temp * s_temp/p
        iminus = gamma * i_temp
        if seir:
            eminus = a * e_temp
            e_temp += sminus - eminus
            i_temp += eminus - iminus
        else:
            i_temp += sminus - iminus
        if lethal:
            rminus = iminus * l
            l_temp += rminus
            r_temp += iminus - rminus
            yl += [l_temp]
        else:
            r_temp += iminus
        if vitalD:
            s_temp += p * mu - s_temp * lam
            e_temp -= e_temp * lam
            r_temp -= r_temp * lam
            i_temp -= i_temp * lam
        s_temp -= sminus
        if s_temp < 0 : 
            s_temp = 0
        yi += [i_temp]
        ys += [s_temp]
        yr += [r_temp]
        ye += [e_temp]
        d += 1
    # returning all y values for I, S, R, E and L if lethal = TRUE
    if lethal:
        return yi, ys, yr, ye,yl
    else:
        return yi, ys, yr, ye
    

# x-values 
counter = 0
x = []
while counter < maxd + 1:
    x += [counter]
    counter += 1

# a, beta and gamma values for the 4 graphs
a1 = 0.5
beta1 = 0.03
gamma1 = 0.01

a2 = 0.2
beta2 = 0.03
gamma2 = 0.01

a3 = 0.5
beta3 = 0.06
gamma3 = 0.01

a4 = 0.2
beta4 = 0.06
gamma4 = 0.01

# population size
p = 80000000
'''
    which setup you want:
    0 : Comparing SEIR with SIR without lethality
    1 : SEIR model
    2 : All graphs with different a values
    3 : SEIR with lethality
    4 : Comparing SEIR with and without vital Dynamics
    5 : SIR model
'''
mode = 3

plt.style.use('bmh')
if mode != 2:
    fig = plt.figure()

    plt1 = fig.add_subplot(221) 
    plt2 = fig.add_subplot(222) 
    plt3 = fig.add_subplot(223) 
    plt4 = fig.add_subplot(224)

if mode == 0:
    i1, s1, r1, e1 = model_SIR(a1, beta1, gamma1, True, False, False)
    i11, s11, r11, e11 = model_SIR(a1, beta1, gamma1, False, False, False)
    plt1.plot(x, i1, label = "Infectious(SEIR)", color = 'red')
    plt1.plot(x, e1, label = "Exposed(SEIR)", color = 'orange')
    plt1.plot(x, i11, label = "Infectious(SIR)", color = 'red', linestyle = 'dashed')
    plt1.set_title(f'R: {beta1 / gamma1}, beta: {beta1}, gamma: {gamma1}, a: {a1}', fontsize=20)

    i2, s2, r2, e2 = model_SIR(a2, beta2, gamma2, True, False, False)
    i22, s22, r22, e22 = model_SIR(a2, beta2, gamma2, False, False, False)
    plt2.plot(x, i2, label = "Infectious(SEIR)", color = 'red')
    plt2.plot(x, e2, label = "Exposed(SEIR)", color = 'orange')
    plt2.plot(x, i22, label = "Infectious(SIR)", color = 'red', linestyle = 'dashed')
    plt2.set_title(f'R: {beta2 / gamma2}, beta: {beta2}, gamma: {gamma2}, a: {a2}', fontsize=20)

    i3, s3, r3, e3 = model_SIR(a3, beta3, gamma3, True, False, False)
    i33, s33, r33, e33 = model_SIR(a3, beta3, gamma3, False, False, False)
    plt3.plot(x, i3, label = "Infectious(SEIR)", color = 'red')
    plt3.plot(x, e3, label = "Exposed(SEIR", color = 'orange')
    plt3.plot(x, i33, label = "Infectious(SIR)", color = 'red', linestyle = 'dashed')
    plt3.set_title(f'R: {beta3 / gamma3}, beta: {beta3}, gamma: {gamma3}, a: {a3}', fontsize=20)

    i4, s4, r4, e4 = model_SIR(a4, beta4, gamma4, True, False, False)
    i44, s44, r44, e44 = model_SIR(a4, beta4, gamma4, False, False, False)
    plt4.plot(x, i4, label = "Infectious(SEIR)", color = 'red')
    plt4.plot(x, e4, label = "Exposed(SEIR)", color = 'orange')
    plt4.plot(x, i44, label = "Infectious(SIR)", color = 'red', linestyle = 'dashed')
    plt4.set_title(f'R: {beta4 / gamma4}, beta: {beta4}, gamma: {gamma4}, a: {a4}', fontsize=20)
elif mode == 1:
    i1, s1, r1, e1 = model_SIR(a1, beta1, gamma1, True, False, False)
    plt1.plot(x, i1, label = "Infectious", color = 'red')
    plt1.plot(x, e1, label = "Exposed", color = 'orange')
    plt1.plot(x, r1, label = "Recovered", color = 'green')
    plt1.plot(x, s1, label = "Susceptible", color = 'blue')
    plt1.set_title(f'R: {beta1 / gamma1}, beta: {beta1}, gamma: {gamma1}, a: {a1}', fontsize=20)

    i2, s2, r2, e2 = model_SIR(a2, beta2, gamma2, True, False, False)
    plt2.plot(x, i2, label = "Infectious", color = 'red')
    plt2.plot(x, e2, label = "Exposed", color = 'orange')
    plt2.plot(x, r2, label = "Recovered", color = 'green')
    plt2.plot(x, s2, label = "Susceptible", color = 'blue')
    plt2.set_title(f'R: {beta2 / gamma2}, beta: {beta2}, gamma: {gamma2}, a: {a2}', fontsize=20)

    i3, s3, r3, e3 = model_SIR(a3, beta3, gamma3, True, False, False)
    plt3.plot(x, i3, label = "Infectious", color = 'red')
    plt3.plot(x, e3, label = "Exposed", color = 'orange')
    plt3.plot(x, r3, label = "Recovered", color = 'green')
    plt3.plot(x, s3, label = "Susceptible", color = 'blue')
    plt3.set_title(f'R: {beta3 / gamma3}, beta: {beta3}, gamma: {gamma3}, a: {a3}', fontsize=20)

    i4, s4, r4, e4 = model_SIR(a4, beta4, gamma4, True, False, False)
    plt4.plot(x, i4, label = "Infectious", color = 'red')
    plt4.plot(x, e4, label = "Exposed", color = 'orange')
    plt4.plot(x, r4, label = "Recovered", color = 'green')
    plt4.plot(x, s4, label = "Susceptible", color = 'blue')
    plt4.set_title(f'R: {beta4 / gamma4}, beta: {beta4}, gamma: {gamma4}, a: {a4}', fontsize=20)
elif mode == 2:
    fig = plt.figure()

    plt1 = fig.add_subplot(211) 
    plt2 = fig.add_subplot(212)
    # initializing all graphs with own a values
    acc = 0
    ac = [0]
    while acc < 100:
        acc += 1
        ac += [acc / 100]
    tdiff = [0]
    hdiff = [0]
    betac = 0.03
    gammac = 0.01
    betac1 = 0.06
    gammac1 = 0.01
    acounter = 0
    while acounter < 100:
        ic, sc, rc, ec = model_SIR(ac[acounter], betac, gammac, True, False, False)
        ic1, sc, rc, ec = model_SIR(ac[acounter], betac1, gammac1, True, False, False)
        if 0.2 < ac[acounter] < 0.5:
            plt1.plot(x, ic, color = 'red')
            plt2.plot(x, ic1, color = 'red')
        else:
            plt1.plot(x, ic)
            plt2.plot(x, ic1)
        acounter += 1
    plt1.set_title(f'R: {betac / gammac}, beta: {betac}, gamma: {gammac}', fontsize=20)
    plt2.set_title(f'R: {betac1 / gammac1}, beta: {betac1}, gamma: {gammac1}', fontsize=20)
    plt.legend()
elif mode == 3:
    i1, s1, r1, e1, l1 = model_SIR(a1, beta1, gamma1, True, True, False)
    plt1.plot(x, i1, label = "Infectious", color = 'red')
    plt1.plot(x, e1, label = "Exposed", color = 'orange')
    plt1.plot(x, s1, label = "Susceptible", color = 'blue')
    plt1.plot(x, r1, label = "Recovered", color = 'green')
    plt1.plot(x, l1, label = "Dead", color = 'black')
    plt1.set_title(f'R: {beta1 / gamma1}, beta: {beta1}, gamma: {gamma1}, a: {a1}', fontsize=20)

    i2, s2, r2, e2, l2 = model_SIR(a2, beta2, gamma2, True, True, False)
    plt2.plot(x, i2, label = "Infectious", color = 'red')
    plt2.plot(x, e2, label = "Exposed", color = 'orange')
    plt2.plot(x, s2, label = "Susceptible", color = 'blue')
    plt2.plot(x, r2, label = "Recovered", color = 'green')
    plt2.plot(x, l2, label = "Dead", color = 'black')
    plt2.set_title(f'R: {beta2 / gamma2}, beta: {beta2}, gamma: {gamma2}, a: {a2}', fontsize=20)

    i3, s3, r3, e3, l3 = model_SIR(a3, beta3, gamma3, True, True, False)
    plt3.plot(x, i3, label = "Infectious", color = 'red')
    plt3.plot(x, e3, label = "Exposed", color = 'orange')
    plt3.plot(x, s3, label = "Susceptible", color = 'blue')
    plt3.plot(x, r3, label = "Recovered", color = 'green')
    plt3.plot(x, l3, label = "Dead", color = 'black')
    plt3.set_title(f'R: {beta3 / gamma3}, beta: {beta3}, gamma: {gamma3}, a: {a3}', fontsize=20)

    i4, s4, r4, e4, l4 = model_SIR(a4, beta4, gamma4, True, True, False)
    plt4.plot(x, i4, label = "Infectious", color = 'red')
    plt4.plot(x, e4, label = "Exposed", color = 'orange')
    plt4.plot(x, s4, label = "Susceptible", color = 'blue')
    plt4.plot(x, r4, label = "Recovered", color = 'green')
    plt4.plot(x, l4, label = "Dead", color = 'black')
    plt4.set_title(f'R: {beta4 / gamma4}, beta: {beta4}, gamma: {gamma4}, a: {a4}', fontsize=20)
elif mode == 4:
    i1, s1, r1, e1, l1 = model_SIR(a1, beta1, gamma1, True, True, False)
    i11, s11, r11, e11, l11 = model_SIR(a1, beta1, gamma1, True, True, True)
    plt1.plot(x, i1, label = "Infectious without vitalD", color = 'red')
    plt1.plot(x, i11, label = "Infectious with vitalD", color = 'red', linestyle = 'dashed')
    plt1.set_title(f'R: {beta1 / gamma1}, beta: {beta1}, gamma: {gamma1}, a: {a1}', fontsize=20)

    i2, s2, r2, e2, l2 = model_SIR(a2, beta2, gamma2, True, True, False)
    i22, s22, r22, e22, l22 = model_SIR(a2, beta2, gamma2, True, True, True)
    plt2.plot(x, i2, label = "Infectious without vitalD", color = 'red')
    plt2.plot(x, i22, label = "Infectious with vitalD", color = 'red', linestyle = 'dashed')
    plt2.set_title(f'R: {beta2 / gamma2}, beta: {beta2}, gamma: {gamma2}, a: {a2}', fontsize=20)

    i3, s3, r3, e3, l3 = model_SIR(a3, beta3, gamma3, True, True, False)
    i33, s33, r33, e33, l33 = model_SIR(a3, beta3, gamma3, True, True, True)
    plt3.plot(x, i3, label = "Infectious without vitalD", color = 'red')
    plt3.plot(x, i33, label = "Infectious with vitalD", color = 'red', linestyle = 'dashed')
    plt3.set_title(f'R: {beta3 / gamma3}, beta: {beta3}, gamma: {gamma3}, a: {a3}', fontsize=20)

    i4, s4, r4, e4, l4 = model_SIR(a4, beta4, gamma4, True, True, False)
    i44, s44, r44, e44, l44 = model_SIR(a4, beta4, gamma4, True, True, True)
    plt4.plot(x, i4, label = "Infectious without vitalD", color = 'red')
    plt4.plot(x, i44, label = "Infectious with vitalD", color = 'red', linestyle = 'dashed')
    plt4.set_title(f'R: {beta4 / gamma4}, beta: {beta4}, gamma: {gamma4}', fontsize=20)
elif mode == 5:
    i1, s1, r1, e1 = model_SIR(a1, beta1, gamma1, False, False, False)
    plt1.plot(x, i1, label = "Infectious", color = 'red')
    plt1.plot(x, r1, label = "Recovered", color = 'green')
    plt1.plot(x, s1, label = "Susceptible", color = 'blue')
    plt1.set_title(f'R: {beta1 / gamma1}, beta: {beta1}, gamma: {gamma1}', fontsize=20)

    i2, s2, r2, e2 = model_SIR(a2, beta2, gamma2, False, False, False)
    plt2.plot(x, i2, label = "Infectious", color = 'red')
    plt2.plot(x, r2, label = "Recovered", color = 'green')
    plt2.plot(x, s2, label = "Susceptible", color = 'blue')
    plt2.set_title(f'R: {beta2 / gamma2}, beta: {beta2}, gamma: {gamma2}', fontsize=20)

    i3, s3, r3, e3 = model_SIR(a3, beta3, gamma3, False, False, False)
    plt3.plot(x, i3, label = "Infectious", color = 'red')
    plt3.plot(x, r3, label = "Recovered", color = 'green')
    plt3.plot(x, s3, label = "Susceptible", color = 'blue')
    plt3.set_title(f'R: {beta3 / gamma3}, beta: {beta3}, gamma: {gamma3}', fontsize=20)

    i4, s4, r4, e4 = model_SIR(a4, beta4, gamma4, False, False, False)
    plt4.plot(x, i4, label = "Infectious", color = 'red')
    plt4.plot(x, r4, label = "Recovered", color = 'green')
    plt4.plot(x, s4, label = "Susceptible", color = 'blue')
    plt4.set_title(f'R: {beta4 / gamma4}, beta: {beta4}, gamma: {gamma4}', fontsize=20)
    

plt.xlabel('days', fontsize=15)
plt.ylabel('people', fontsize = 15)

if mode != 2:
    plt1.legend(prop={'size' : 17})
    plt2.legend(prop={'size' : 17})
    plt3.legend(prop={'size' : 17})
    plt4.legend(prop={'size' : 17})

plt.show()