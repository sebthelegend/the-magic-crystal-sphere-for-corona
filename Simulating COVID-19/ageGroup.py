import matplotlib.pyplot as plt
import numpy as np

'''
    i: start values for I for each age group
    s: start values for S for each age group
    l: lethality rate for each age group
    beta: beta values for each age group, last beta value is the generall beta value
    maxd: time period
'''
i = [0,100,200,50]
s = [10530000 - i[0], 15530000 - i[1], 33580000 - i[2], 23370000 - i[3]]
l = [0,0,0.004,0.133]
beta = [0.03,0.06,0.05,0.02, 0.02]
maxd = 1000

# calculating population for each age group, last counter is the sum of all
p = [0,0,0,0,0]
counter = 0
while counter < 4:
    p[counter] += s[counter] + i[counter]
    p[4] += s[counter] + i[counter]
    counter += 1

'''
    function: SEIR model with simulating specific age groups

    float a : period between infected and infectious
    float gamma : period of I
'''
def model_SEIRAge(a, gamma):
    # initializing all variables and lists
    d = 1
    i_temp = i.copy()
    s_temp = s.copy()
    r_temp = [0,0,0,0,0]
    e_temp = [0,0,0,0,0]
    l_temp = [0,0,0,0,0]
    ys = [[s_temp[0]],[s_temp[1]],[s_temp[2]],[s_temp[3]]]
    ye = [[0],[0],[0],[0]]
    yi = [[i_temp[0]],[i_temp[1]],[i_temp[2]],[i_temp[3]]]
    yr = [[0],[0],[0],[0]]
    yl = [[0],[0],[0],[0]]
    adds = [s_temp[0] + s_temp[1] + s_temp[2] + s_temp[3]]
    adde = [0]
    addi = [i_temp[0] + i_temp[1] + i_temp[2] + i_temp[3]]
    addr = [0]
    addl = [0]
    # loop through all days
    while d < maxd:
        counter = 0
        # sum of all age groups
        addi += [0]
        adds += [0]
        addr += [0]
        adde += [0]
        addl += [0]
        # loop through all age groups
        while counter < 4:
            # calculating S,E,I,R and L for each age group
            # people infected from inside the age group + people infected from outside of the age group
            sminus = beta[counter] * i_temp[counter] * s_temp[counter]/p[counter] + beta[4] * (addi[d - 1] - i_temp[counter])/4 * s_temp[counter]/p[counter]
            iminus = gamma * i_temp[counter]
            eminus = a * e_temp[counter]
            e_temp[counter] += sminus - eminus
            i_temp[counter] += eminus - iminus
            rminus = iminus * l[counter]
            l_temp[counter] += rminus
            r_temp[counter] += iminus - rminus
            s_temp[counter] -= sminus
            if s_temp[counter] < 0 : 
                s_temp[counter] = 0
            yi[counter] += [i_temp[counter]]
            ys[counter] += [s_temp[counter]]
            yr[counter] += [r_temp[counter]]
            ye[counter] += [e_temp[counter]]
            yl[counter] += [l_temp[counter]]
            addi[d] += yi[counter][d]
            adds[d] += ys[counter][d]
            addr[d] += yr[counter][d]
            adde[d] += ye[counter][d]
            addl[d] += yl[counter][d]
            counter += 1
        d += 1
    y1 = [yi[0], ye[0], ys[0], yr[0], yl[0]]
    y2 = [yi[1], ye[1], ys[1], yr[1], yl[1]]
    y3 = [yi[2], ye[2], ys[2], yr[2], yl[2]]
    y4 = [yi[3], ye[3], ys[3], yr[3], yl[3]]
    addres = [addi, adds, addr, adde, addl]
    # returning all [I, E, S, R, L] for each age group and the sum of all groups
    return y1, y2, y3, y4, addres

# standard SEIR model please refer to program SEIR-model
def model_SEIR(a, beta, gamma):
    d = 1
    l = 0.047
    p = 83000000
    i_temp = 350
    s_temp = 83000000 - i_temp
    r_temp = 0
    e_temp = 0
    l_temp = 0
    yl = [l_temp]
    yi = [i_temp]
    ys = [s_temp]
    yr = [r_temp]
    ye = [e_temp]
    while d < 1000:
        sminus = beta * i_temp * s_temp/p
        iminus = gamma * i_temp
        eminus = a * e_temp
        e_temp += sminus - eminus
        i_temp += eminus - iminus
        rminus = iminus * l
        l_temp += rminus
        r_temp += iminus - rminus
        yl += [l_temp] 
        s_temp -= sminus
        if s_temp < 0 : 
            s_temp = 0
        yi += [i_temp]
        ys += [s_temp]
        yr += [r_temp]
        ye += [e_temp]
        d += 1
    return yi, ys, yr, ye,yl


plt.style.use('bmh')

fig = plt.figure()

plt1 = fig.add_subplot(211)
plt2 = fig.add_subplot(212)

# initializing a, gamma and beta
a = 0.5
gamma = 0.01
beta2 = 0.06

# x values
counter = 0
x = []
while counter < maxd:
    x += [counter]
    counter += 1

y1, y2, y3, y4, ins = model_SEIRAge(a, gamma)
plt1.plot(x, ins[0], label = "all", color = 'red', linestyle = '--')
plt1.plot(x, y1[0], label = "0-10")
plt1.plot(x, y2[0], label = "11-30")
plt1.plot(x, y3[0], label = "31-60")
plt1.plot(x, y4[0], label = "61-")
plt1.set_title(f'gamma: {gamma}, a: {a}', fontsize=20)

i1, s1, r1, e1, l1 = model_SEIR(a, beta2, gamma)

plt2.plot(x, ins[0], label = 'I with age division', color = 'red', linestyle = '--')
plt2.plot(x, i1, label = 'I without age division', color = 'red', linestyle = '-')
plt2.plot(x, ins[4], label = 'L with age division', color = 'black', linestyle = '--')
plt2.plot(x, l1, label = 'L without age division', color = 'black', linestyle = '-')
plt2.set_title(f'R: 6, beta: 0.06, gamma: {gamma}, a: {a}', fontsize=20)

plt.xlabel('days', fontsize=15)
plt.ylabel('people', fontsize = 15)
plt1.legend(prop={'size' : 17})
plt2.legend(prop={'size' : 17})
plt.show()